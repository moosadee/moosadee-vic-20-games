001rem*move character
002rem*x,y: pos
003rem*lx,ly: lastpos
004rem*ix,iy: input
005rem*for gosub

010rem***************
011rem* init
012 x=0: y=0
013 up=9: down=41
014 left=17: right=18
015 w=21: h=22
016 av=81: col=0
017 mpt=32

030rem***************
031rem* get input
032 lx=x:ly=y
033 k=peek(197)
034 if k=up    then    y=y-1
035 if k=down  then    y=y+1
036 if k=left  then    x=x-1
037 if k=right then    x=x+1

040rem***************
041rem* keep on scrn
042 if y<0 then y=0
043 if y>h then y=h
044 if x<0 then x=0
045 if x>w then x=w

050rem***************
051rem* clear prev loc
052 if x<>lx or y<>ly  then 60
053 goto 70

060rem ** if **
061 ix=lx:iy=ly
062 gosub 800
063 poke p,mpt

070rem ** else **
071rem***************
072rem* draw
073 ix=x:iy=y
074 gosub 800
075 gosub 900
076 poke c, col
077 poke p, av

080rem***************
081rem* loop
082 goto 30

090 end

800rem***************
801rem* char memloc
802 p=7680+iy*22+ix
803 return

900rem***************
901rem* color memloc
902 c=38400+iy*22+ix
903 return
