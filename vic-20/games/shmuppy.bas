
010 rem goto 100

020 rem title----------
021 gosub 1300 : rem clear screen
022 print "shmuppy"
023 print ""
024 print "this is a bad game"
025 print "use a and d to move"
026 print "you can break the blox"
027 print ""

030 print "press any key"
031 get k$ : if k$="" then 31



100 rem init --------
101 print ""
102 print "wait..."

103 gosub 1300 : rem clear screen

110 rem screen ------
111 w=21: h=22

120 rem player ------
121 px=w/2: py=20: k=17
124 lx=px:rem last x
125 ly=py:rem last y

126 rem bullet ------
127 sx=w/2: sy=h/2: vx=0: vy=-1

130 rem args --------
131 ix=0 :rem arg x
132 iy=0 :rem arg y

133 rem blocks -------
134 dim ex(w)
135 dim ey(w)

136 for i=0 to w
137 ex(i)=i : ey(i)=int(i/3)

149 rem draw block
151 ix=ex(i): iy=ey(i)
152 gosub 1400
153 gosub 1500
154 poke c, 5
155 poke p, 98
156 rem poke p-2, 81
157 rem poke p+2, 81
160 next i

170 rem draw player
171 ix=px:iy=py
172 gosub 1400
173 gosub 1500
174 poke c,3
175 poke p,65
176 poke p-1,65
177 poke p+1,65

200 rem




250 rem update
252 k=peek(197) : rem (turn based) if k=64 then 252
253 x=0

260 rem player move
261 if k=17               then x=x-1  :rem west
262 if k=18               then x=x+1  :rem east

263 rem clear last        position
264 if k<>17 and k<>18 then 300 : rem skip draw
265 ix=px:iy=py
266 gosub 1400      :     rem call memloc       symbol
267 gosub 1500      :     rem call memloc       color
268 poke c,1
269 poke p,32 : poke p-1,32 : poke p+1,32

270 rem move player
271 px=px+x
281 if px<0 then px=w
282 if px>w then px=0

290 rem draw player
291 ix=px:iy=py
292 gosub 1400
293 gosub 1500
294 poke c,3
295 poke p,65
296 poke p-1,65
297 poke p+1,65


300 rem shoot bullet
301 rem print k
302 if k=9 and sy=-1 then sx=px : sy=py-1

310 rem has bullet?
311 if sy=-1 then 400 : rem skip bullet update

320 rem clear last bullet loc
321 ix=sx:iy=sy
322 gosub 1400
323 gosub 1500
324 poke c, 0
325 poke p, 32

330 rem move bullet
331 sy=sy+vy        :     rem move bullet
332 sx=sx+vx

340 rem draw bullet
341 ix=sx:iy=sy
342 gosub 1400      :     rem call memloc       symbol
343 gosub 1500      :     rem call memloc       color

345 if peek(p)=98 or sy=0 or sy=h then vy=-vy
346 poke c, 7
347 poke p, 42
350 rem ix=sx:iy=sy
351 rem poke c, 7
352 rem poke p, 42

360 sx=int(sx) : sy=int(sy) : px=int(px) : py=int(py)
361 rem hit screen edge?
362 if sx=0 or sx=w then : vx=-vx

370 rem hit player?
371 if sx=px and sy=py then vx=0: vy=-vy
372 if sx=px-1 and sy=py then vx=-1: vy=-vy
373 if sx=px+1 and sy=py then vx=1: vy=-
374 rem

455 rem ix=px:iy=py
456 rem gosub 1400l
457 rem gosub 1500
458 rem poke c,1
459 rem poke p,32 : poke p-1,32 : poke p+1,32


500 goto 250           :   rem loop

1300 rem clear screen
1301 poke 36879, 14
1302 for i=1 to 22   :     print ""        :     next i
1303 return


1400 rem memloc sym
1401 p=7680+iy*22+ix
1402 return


1500 rem memloc col
1501 c=38400+iy*22+ix
1502 return



2200 end

