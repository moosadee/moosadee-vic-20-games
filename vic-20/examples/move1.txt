1 rem clear screen
2 rem before run

10 rem ***************
11 rem init
12 x=0: y=0
13 w=22: h=23
14 ball=81

20 rem ***************
21 rem draw
22 gosub 100: gosub 200
23 poke c, col
24 poke p, ball

30 rem ***************
31 rem move
32 x=x+1
33 if x>w then y=y+1: x=0: col=col+1

40 rem ***************
41 rem loop
42 goto 20

90 end

100 rem *************
101 rem char memloc
102 p=7680+y*22+x
103 return

200 rem *************
201 rem color memloc
202 c=38400+y*22+x
203 return
