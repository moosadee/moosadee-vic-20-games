1 rem colorcodes

10 x=0: y=0
11 w=22: h=23
12 ball=81

30 for col=0 to 255
31  rem char/col mem
32  gosub 100
33  gosub 200
40  rem set color
41  poke c, col
42  rem set symbol
43  poke p, ball

50 rem move x
51 x=x+1
52 if x>w then x=0:y=y+1

70 next col

80 end

100 rem *************
101 rem char memloc
102 p=7680+y*22+x
103 return

200 rem *************
201 rem color memloc
202 c=38400+y*22+x
203 return
