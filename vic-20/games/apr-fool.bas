001rem*move character
002rem*px,py:plyrpos
003rem*lx,ly:lastpos
004rem*ix,iy:input
005rem*for gosub
006rem*sx,sy:stickpos
007rem*scol:stickcolor
008rem*sav:stickav

010rem***************
011rem* init
012 px=11: py=11
013 up=9: down=41
014 left=17: right=18
015 space=32
016 w=21: h=22
017 pav=81: pcol=0
018 mpt=32
019 sx=0: sy=0
020 scol=2: sav=115
021 gosub 700
022 bgcol=5
023 scr=0
024 want=9
025 poke 36878,5

030rem***************
031rem* title
031 for i=1 to 22 : print : next i
032 print "kolektu brancxojn" : print
033 print "de verdprogramisto" : print : print
034 print "---------------------" : print : print
035 print "vi estas dika hundo"
036 print "kaj vi volas atingi"
037 print want ; "brancxojn!" : print : print
038 print "premu enter"
039 input z : scr=0

050rem***************
051rem* clear scrn
051print "!! bonvolu atendi !!"
052 for c=0 to 505
053  poke 7680+c,mpt
054 next c
055rem* border/color
056 poke 36879,94

130rem***************
131rem* get input
132 rem
133 lx=px:ly=py
134 k=peek(197)
135 if k=up    then    py=py-1
136 if k=down  then    py=py+1
137 if k=left  then    px=px-1
138 if k=right then    px=px+1
139 if k=space then gosub 600

140rem***************
141rem* keep on scrn
142 if py<0 then py=0
143 if py>h then py=h
144 if px<0 then px=0
145 if px>w then px=w

150rem***************
151rem* get stick?
152 if px=sx and py=sy then 160
153 goto 250

160rem** if *********
161rem*new stick pos
163 gosub 700
164 scr=scr+1
165 if scr=want goto 1000
166 goto 250

250rem***************
251rem* clear prev loc
252 if px<>lx or py<>ly  then 260
253 goto 270

260rem*clear prev loc*
261 ix=lx:iy=ly
262 gosub 800: gosub 900
263 poke c,bgcol
264 poke p,mpt

270rem***************
272rem*draw
273 rem draw player
274 ix=px:iy=py
275 gosub 800
276 gosub 900
277 poke c, pcol
278 poke p, pav

280rem***************
281rem*draw stick
282 ix=sx:iy=sy
283 gosub 800
284 gosub 900
285 poke c, 2
286 poke p, sav

290rem***************
291rem*draw score
292 ix=0: iy=0
293 gosub 800: gosub 900
294 poke c, 0
295 poke p, 48+scr
296 ix=1: iy=0
297 gosub 800: gosub 900
298 poke c, 0
299 poke p, 78
300 ix=2: iy=0
301 gosub 800: gosub 900
302 poke c, 0
303 poke p, 48+want

380rem***************
381rem* loop
382 goto 130

399 end

600rem***************
601rem*debug
602 print "p:";px;py;"col:";pcol
603 print "s:";sx;sy;"col:";scol

700rem***************
701rem* rnd stickpos
702 sx=int(rnd(1)*w)
703 sy=int(rnd(1)*h)+1
704 return

800rem***************
801rem* char memloc
802 p=7680+iy*22+ix
803 return

900rem***************
901rem* color memloc
902 c=38400+iy*22+ix
903 return

1000rem***************
1001rem* win
1002 for i=1 to 22 : print : next i
1003 print "vi sukcesis!!" : print
1004 print "mi gratulas vin!" : print : print
1005 print "---------------------" : print : print
1006 print "se vi volas ludi denove"
1007 print "premu enter!" : print : print
1008 input z
1010 goto 001





