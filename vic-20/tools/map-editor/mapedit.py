import pygame, sys
from pygame.locals import * 

pygame.init()
fps = pygame.time.Clock()

vicwidth = 22
vicheight = 23
charwh = 7

window = pygame.display.set_mode( ( 640, 480 ), HWSURFACE | DOUBLEBUF | RESIZABLE )
pygame.display.set_caption( "editor" )

tileset = pygame.image.load( "characters.png" )
bgcol = pygame.Color( 200, 200, 200 )

tiles = []
code = 7680

current_brush = 0

for y in range( vicheight ):
    for x in range( vicwidth ):
        tile = {}
        tile["memcode"] = code
        tile["x"] = x
        tile["y"] = y
        tile["w"] = charwh
        tile["h"] = charwh
        tile["frame"] = 0
        code = code+1
        tiles.append( tile )

mx=0
my=0
tilex = 0
tiley = 0

while True:
    window.fill( bgcol )
    
    
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()
            
        elif ( event.type == MOUSEBUTTONDOWN ):
            pass
            
        elif ( event.type == MOUSEMOTION ):
            mx, my = event.pos
            tilex = mx / charwh
            tiley = my / charwh
            
        elif ( event.type == KEYDOWN ):
            if ( event.key == K_EQUALS ):
                current_brush = current_brush + 1
                if ( current_brush > 127 ): current_brush = 0
                
            elif ( event.key == K_MINUS ):
                current_brush = current_brush - 1
                if ( current_brush < 0 ): current_brush = 127
    
    # tileset
    window.blit( tileset, ( 0, 300 ) )
    
    # tiles
    for tile in tiles:
        rect = pygame.Rect( tile["frame"] * tile["w"], 0, tile["w"], tile["h"] )
        window.blit( tileset, ( tile["x"] * tile["w"], tile["y"] * tile["h"] ), rect )   
        
    # mouse
    brushrect = pygame.Rect( current_brush * charwh, 0, charwh, charwh )
    window.blit( tileset, ( int( mx/charwh )*charwh, int( my/charwh )*charwh ), brushrect )
            
    pygame.display.update()
    fps.tick( 30 )
