1000 rem ************
1001 print "1: scales"
1002 poke 36878,15
1003 for l=250 to 200 step -2
1004 poke 36876,l
1005 for m=1 to 100
1006 next m
1007 next l
1008 for l=205 to 250 step 2
1009 poke 36876,l
1010 for m=1 to 100
1011 next m
1012 next l
1013 poke 36876,0
1014 poke 36878,0

1020 rem ************
1021 print "2: computer mania"
1023 poke 36878,15
1024 for l=1 to 100
1025 poke 36876, int(rnd(1)*128)+128
1026 for m=1 to 10
1027 next m
1028 next l
1029 poke 36876,0
1030 poke 36878,0

1040 rem ************
1041 print "3: explosion"
1042 poke 36877,220
1043 for l=15 to 0 step -1
1044 poke 36878,l
1045 for m=1 to 300
1046 next m
1047 next l
1048 poke 36877,0
1049 poke 36878,0

1060 rem ************
1061 print "4: bombs away"
1062 poke 36878,10
1063 for l=230 to 128 step -1
1064 poke 36876,l
1065 for m=1 to 20
1066 next m
1067 next l
1068 poke 36876, 0
1069 poke 36877, 200
1070 for l=15 to 0 step -0.05
1071 poke 36878, l
1072 next l
1073 poke 36877, 0

1080 rem ************
1081 print "5: red alert"
1082 poke 36878, 15
1083 for l=1 to 10
1084 for m=180 to 235 step 2
1085 poke 36876, m
1086 for n=1 to 10
1087 next n
1088 next m
1089 poke 36876, 0
1090 for m=1 to 100
1091 next m
1092 next l
1093 poke 36878, 0

1100 rem ************
1101 print "6: laser beam"
1102 poke 36878, 15
1103 for l=1 to 30
1104 for m=250 to 240 step -1
1105 poke 36876, m
1106 next m
1107 for m=240 to 250
1108 poke 36876, m
1109 next m
1110 poke 36876, 0
1111 next l
1112 poke 36878, 0

1120 rem ************
1121 print "7: high-low siren"
1122 poke 36878, 15
1123 for l=1 to 10
1124 poke 36875, 200
1125 for m=1 to 500
1126 next m
1127 poke 36875, 0
1128 poke 36876, 200
1129 for m=1 to 500
1130 next m
1131 poke 36876, 0
1132 next l
1133 poke 36878, 0

1140 rem ************
1141 print "8: busy signal"
1142 poke 36878, 15
1143 for l=1 to 15
1144 poke 36876, 160
1145 for m=1 to 400
1146 next m
1147 poke 36876, 0
1148 for m=1 to 400
1149 next m
1150 next l
1151 poke 36878, 0

1160 rem ************
1161 print "9: phone ringing"
1162 poke 36878, 15
1163 for l=1 to 5
1164 for m=1 to 50
1165 poke 36876, 220
1166 for n=1 to 5
1167 next n
1168 poke 36876, 0
1169 next m
1170 for m=1 to 3000
1171 next m
1172 next l
1173 poke 36878, 0

1180 rem ************
1181 print "10: birds chirping"
1182 poke 36878, 15
1183 for l=1 to 20
1184 for m=254 to 240 + int(rnd(1)*10) step -1
1185 poke 36876, m
1186 next m
1187 poke 36876, 0
1188 for m=0 to int(rnd(1)*100)+120
1189 next m
1190 next l


1200 rem ************
1201 print "11: wind"
1202 poke 36878, 15
1203 poke 36874, 170
1204 poke 36877, 240
1205 for l=1 to 2000
1206 next l
1207 poke 36874, 0
1208 poke 36877, 0
1209 poke 36878, 0


1220 rem ************
1221 print "12: ocean waves"
1222 poke 36877, 180
1223 for l=1 to 10
1224 d=int(rnd(1)*5)*50+50
1225 for m=3 to 15
1226 poke 36878, m
1227 for n=1 to d
1228 next n
1229 next m
1230 next l
1231 poke 36878, 0
1232 poke 36877, 0

1240 rem ************
1241 print "13: vanishing ufo"
1242 poke 36878, 15
1243 for l=130 to 254
1244 poke 36876, l
1245 for m=1 to 40
1246 next m
1247 next l
1248 poke 36878, 0
1249 poke 36876, 0

1260 rem ************
1261 print "14: ufo landing"
1262 poke 36878, 15
1263 for l=1 to 20
1264 for m=220-l to 160-l step -4
1265 poke 36876, m
1266 next m
1267 for m=160-l to 220-l step 4
1268 poke 36876, m
1269 next m
1270 next l
1271 poke 36878, 0
1272 poke 36876, 0

1280 rem ************
1281 print "15: ufo shooting"
1282 poke 36878, 15
1283 for l=1 to 15
1284 for m=200 to 220+l*2
1285 poke 36876, m
1286 next m
1287 next l
1288 poke 36878, 0
1289 poke 36876, 0

1300 rem ************
1301 print "16: wolf whistle"
1302 poke 36878, 15
1303 for l=148 to 220 step .7
1304 poke 36876, l
1305 next l
1306 for l=128 to 200
1307 poke 36876, l
1308 next l
1309 for l=200 to 128 step -1
1310 poke 36876, l
1311 next l
1312 poke 36878, 0
1313 poke 36876, 0

1320 rem ************
1321 print "17: running feet"
1322 poke 36878, 15
1323 for l=1 to 10
1324 poke 36874, 200
1325 for m=1 to 10
1326 next m
1327 poke 36874, 0
1328 for m=1 to 100
1329 next m
1330 next l
1331 poke 36878, 0

1340 rem ************
1341 print "18: tick-tock"
1342 poke 36878, 15
1343 for l=1 to 10
1344 poke 36875, 200
1345 for m=1 to 10
1346 next m
1347 poke 36875, 0
1348 for m=1 to 300
1349 next m
1350 poke 36874, 200
1351 for m=1 to 10
1352 next m
1353 poke 36874, 0
1354 for m=1 to 300
1355 next m
1356 next l
1357 poke 36878, 0

1360 rem ************
1361 print "19: door opening"
1362 poke 36878, 15
1363 b=0
1364 for l=128 to 255 step 11
1365 poke 36874, l
1366 for m=1 to 10
1367 next m
1368 b=b+1
1369 if b=3 then b=0: poke 36874, 0
1370 next l
1371 poke 36874, 0
1372 poke 36878, 0

1380 rem ************
1381 print "20: blips"
1382 poke 36878, 15
1383 poke 36876, 220
1384 for l=1 to 5
1385 next l
1386 poke 36876, 0
1387 for l=1 to 500
1388 next l
1389 poke 36876, 200
1390 for l=1 to 5
1391 next l
1392 poke 36876, 0
1393 for l=1 to 500
1394 next l
1395 poke 36878, 0
